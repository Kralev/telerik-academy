import java.util.Scanner;

public class PrimeNum {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        boolean[] primes = new boolean[n + 1];


        for (int i = 2; i <= n; i++) {

            if (!primes[i]) {

                for (int j = i * i; j <= n; j += i) {
                    primes[j] = true;
                }
            }
        }

        for (int i = n; i >= 2; i--) {

            if (!primes[i]) {

                System.out.println(i);
                break;
            }
        }
    }
}

